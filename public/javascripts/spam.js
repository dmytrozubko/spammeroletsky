const express = require("express");
const app = express();
const bodyParser = require("body-parser");
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());

const urlencodedParser = bodyParser.urlencoded({extended: false});

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/panel.html");
});

app.get("/victim", function (req, res) {
  console.log("Getting all users");
  res.send("All users");
});

app.post('/victim', urlencodedParser, function(req, res) {
  console.log(req.body);
  res.send("Added!");
});

app.put('/victim/:user', function (req, res) {
  console.log(req.body);
  res.send("Updated!");
});

app.delete('/victim/:user', function (req, res) {
  console.log(req.body);
  res.send("Deleted!");
});

app.get('/spam', function (req, res) {
  console.log("Sending emails!");
  res.send("Spam sent!");
});

app.listen(8888);